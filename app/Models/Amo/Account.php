<?php

namespace App\Models\Amo;


class Account extends Model
{
    protected $entity = 'account';


    /**
     * Получение данных аккаунта
     *
     * @param string $params
     * @return array
     */
    public function getInfo($params = '')
    {
        $result = $this->requestGET($this->entity, $params);
        return $result;
    }

}