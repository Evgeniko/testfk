<?php

namespace App\Models\Amo;

class Task extends Model
{
    protected $entity = 'tasks';

    /**
     * Полученпие задач amo
     *
     * @param string $params
     * @param bool $modifed
     * @return array
     */
    public function all($params = '', $modifed = false)
    {
        $result = $this->get($this->entity, $params, $modifed);

        return $result;
    }

    /**
     * Добавление задачи в amo
     *
     * @param $data
     * @return array
     */
    public function add($data)
    {
        $data = ['add' => $data];

        return $this->post($this->entity, $data);
    }
}