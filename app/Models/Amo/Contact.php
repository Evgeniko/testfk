<?php

namespace App\Models\Amo;


class Contact extends Model
{

    protected $entity = 'contacts';

    /**
     * Получение контактов из amo
     *
     * @param string $params
     * @param bool $modifed
     * @return array
     */
    public function all($params = '', $modifed = false)
    {
        return $this->get($this->entity, $params, $modifed);
    }

    /**
     * Добавление новых контактов в амо
     *
     * @param $data
     * @return mixed
     */
    public function add($data)
    {
        $data = ['add' => $data];

        return $this->post($this->entity, $data);
    }

}