<?php

namespace App\Models\Amo;


class Lead extends Model
{
    protected $entity = 'leads';


    /**
     * Получение сделок из amo
     *
     * @param string $params
     * @param bool $modifed
     * @return array
     */
    public function all($params = '', $modifed = false)
    {
        $result = $this->get($this->entity, $params, $modifed);
        return $result;
    }

    /**
     * Добавление новых сделок в amo
     *
     * @param $data
     * @return mixed
     */
    public function add($data)
    {
        $data = ['add' => $data];

        return $this->post($this->entity, $data);
    }

}