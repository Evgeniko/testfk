<?php

namespace App\Models;


class Form
{

     public function validate($data)
     {
        $telephone = $data['tel'];
        $email = $data['email'];

        return $this->telephoneValidate($telephone) && $this->emailValidate($email);
     }

     private function telephoneValidate($telephone)
     {
         if (!$telephone){
             return false;
         }

         return true;
     }

     private function emailValidate($email)
     {
         if (!$email){
             return false;
         }

         return true;
     }

}