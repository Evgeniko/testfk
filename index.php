<?php
header('Access-Control-Allow-Origin: *');
session_start();
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
define('ROOT',__DIR__);

require_once (ROOT.'/bootstrap/app.php');
require_once (ROOT.'/routes/routes.php');


$app->run();

