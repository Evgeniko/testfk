<?php

namespace App\Helpers;


class AmoHelpers
{

    /**
     * Получение id пользователя с наименьшим кол-вом сделок
     * Иначе возвращает false
     *
     * @param array $users
     * @param array $leads
     * @return bool|int
     */
    public static function lowestLeads(array $users, array $leads)
    {
        if (!$users){
            return false;
        }
        //Создаём массив пользователей (не админов)
        $accounts = [];
        foreach ($users as $accountUser){
            if (!$accountUser['is_admin']){
                $id = $accountUser['id'];
                $accounts[$id] = [
                    'contacts' => [],
                    'leads' => 0
                ];
            }
        }

        //Увеличиваем кол-во сделок пользователя, если сделка содержит уникальный контакт
        foreach ($leads as $lead) {
            $responsible_user = $lead['responsible_user_id'];
            $contacts = $lead['contacts'];
            foreach ($contacts as $contact){
                if (array_key_exists($responsible_user, $accounts)){
                    if (!in_array($contact, $accounts[$responsible_user]['contacts'])){
                        $accounts[$responsible_user]['contacts'][] = $contact; //Добавляем контакт в массив к пользователю(для дальнейшей проверки на уникальность)
                        $accounts[$responsible_user]['leads']++;
                    }
                }
            }
        }

        //Получаем пользователя с найименьшим кол-вом сделок
        $ids = array_keys($accounts);
        $result = $ids[0]; //По-умолчанию выбираем 1-го пользователя
        foreach ($accounts as $id => $user){
            //Обходим массив и выбираем id с найименьшим кол-вом сделок
            if ($user['leads'] < $accounts[$result]['leads']){
                $result = $id;
            }
        }

        return (int)$result;
    }


    /**
     * Поиск контакта по номеру и по email
     * Иначе возвращает false
     *
     * @param array $contacts
     * @param bool $telephone
     * @param bool $email
     * @return array|bool
     */
    public static function findContactByTelephoneOrEmail(array $contacts = [], $telephone = false, $email = false)
    {
        if (!$contacts || (!$telephone || !$email)){
            return false;
        }

        $resultFindByEmail = self::findContactByEmail($contacts, $email);
        $resultFindByTelephone = self::findContactByTelephone($contacts, $telephone);

        if (!$resultFindByEmail && !$resultFindByTelephone){
            return false;
        }else if ($resultFindByEmail){
            return $resultFindByEmail;
        }else if ($resultFindByTelephone){
            return $resultFindByTelephone;
        }

        return false;
    }

    /**
     * Поиск контакта по номеру телефона
     * Иначе возвращает false
     *
     * @param array $contacts
     * @param bool $telephone
     * @return bool|array
     */
    public static function findContactByTelephone(array $contacts = [], $telephone = false)
    {
        if (!$contacts || !$telephone){
            return false;
        }

        $telephone = preg_replace('/[^0-9]/', '', $telephone);
        foreach ($contacts as $contact){
            $custom_fields = $contact['custom_fields'];
            foreach ($custom_fields as $field){
                $name = $field['name'];
                if ($name == "Телефон"){
                    $values = $field['values'];
                    foreach ($values as $value){
                        $value = preg_replace('/[^0-9]/', '', $value['value']);
                        if ($value == $telephone){
                            return $contact;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Поиск контакта по email
     * Иначе возвращает false
     *
     * @param array $contacts
     * @param bool $email
     * @return bool|array
     */
    public static function findContactByEmail(array $contacts = [], $email = false)
    {
        if (!$contacts || !$email){
            return false;
        }

        foreach ($contacts as $contact){
            $custom_fields = $contact['custom_fields'];
            foreach ($custom_fields as $field){
                $name = $field['name'];
                if ($name == "Email"){
                    $values = $field['values'];
                    foreach ($values as $value){
                        if ($value['value'] == $email){
                            return $contact;
                        }
                    }
                }
            }
        }

        return false;
    }

}