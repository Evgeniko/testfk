<?php

namespace App\Controllers;


use App\Helpers\AmoHelpers;
use App\Models\Form;
use Interop\Container\ContainerInterface;

class FormController extends Controller
{
    protected $amoSettings;
    protected $contactModel;
    protected $accountModel;
    protected $leadModel;
    protected $taskModel;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->amoSettings = $this->container->get('settings')['amo'];
        $this->contactModel = $this->container->get('ContactModel');
        $this->accountModel = $this->container->get('AccountModel');
        $this->leadModel = $this->container->get('LeadModel');
        $this->taskModel = $this->container->get('TaskModel');
    }

    /**
     * Отображение страницы формы
     *
     * @return mixed
     */
    public function view()
    {
        $page = require_once ROOT."/views/form.html";

        return $page;
    }

    /**
     * Обработка приянтой формы с сайта
     *
     * @param $request
     * @param $response
     * @param $args
     * @return string
     */
    public function submit($request, $response, $args)
    {
        $data = $request->getParsedBody();
        if (!$this->validate($data)){
            return "Некоторые поля были заполнены неверно";
        }
        $nameInput = !empty($data['name'])? $data['name'] : "Не указано";
        $telephoneInput = $data['tel'];
        $emailInput = $data['email'];

        mail($this->container->get('settings')['admin_mail'], 'Заявка с сайта', "
            Имя: $nameInput \n
            Телефон: $telephoneInput \n
            Email: $emailInput \n
        ");

        $contactsAmo = $this->contactModel->all();

        //Если найден контакт с такими же данными
        if ($foundContact = AmoHelpers::findContactByTelephoneOrEmail($contactsAmo, $telephoneInput, $emailInput)){
            $foundContactId = $foundContact['id'];
            $foundResponsibleUserId = $foundContact['responsible_user_id'];
            $addedLead = $this->addLead($foundResponsibleUserId, $foundContactId)[0];
            $addedTask = $this->addTask($foundResponsibleUserId, $addedLead['id']);
        //Если контакта не существует
        }else {
            $accountUsers = $this->accountModel->getInfo('with=users')['users']; //Получаем пользователей
            $leadsLast24Hour = $this->leadModel->all('', mktime(0,0,0)); //Получаем сделки за последние 24 часа
            $responsibleIdLowestLeads = AmoHelpers::lowestLeads($accountUsers, $leadsLast24Hour); //Получаем менеджера с наименьшим кол-вом сделок или false
            $addedContact = $this->addContact($responsibleIdLowestLeads, $nameInput, $telephoneInput, $emailInput)[0]; //Добавляем контакт
            $addedContactId = $addedContact['id'];
            $addedLead = $this->addLead($responsibleIdLowestLeads, $addedContactId)[0];
            $addedTask = $this->addTask($responsibleIdLowestLeads, $addedLead['id']);
        }

        return "<h1>Успешно, мы свяжемся с вами в ближайшее время</h1>";
    }

    /**
     * Добавление сделки в amo
     *
     * @param $responsibleUserId
     * @param $contactId
     * @return mixed
     */
    private function addLead($responsibleUserId, $contactId)
    {
        return $this->leadModel->add([
               [
                   'name' => "Заявка с сайта",
                   'responsible_user_id' => $responsibleUserId,
                   'contacts_id' => $contactId,
               ]
            ]);
    }

    /**
     * Добавление контакта в amo
     *
     * @param integer $responsibleUserId
     * @param string $name
     * @param string $telephone
     * @param string $email
     * @return mixed
     */
    private function addContact($responsibleUserId, $name = "Не указано", $telephone = "", $email = "")
    {
        $telephoneFieldId = $this->amoSettings['telephoneFieldId'];
        $emailFieldId = $this->amoSettings['emailFieldId'];
        return $this->contactModel->add([
            [
                'name' => $name,
                'responsible_user_id' => $responsibleUserId,
                'custom_fields' => [
                    [
                        'id' => $telephoneFieldId,
                        'values' => [
                            [
                                'value' => $telephone,
                                'enum' => "WORK"
                            ]
                        ]
                    ],
                    [
                        'id' => $emailFieldId,
                        'values' => [
                            [
                                'value' => $email,
                                'enum' => "WORK"
                            ]
                        ]
                    ]
                ]
            ]
        ]);
    }

    /**
     * Добавление задачи в amo
     *
     * @param $elementId
     * @return mixed
     */
    private function addTask($responsibleUserId, $elementId)
    {
        return $this->taskModel->add([
            [
                'responsible_user_id' => $responsibleUserId,
                'element_id' => $elementId,
                'element_type' => 2,
                'complete_till_at' => time() + 24*60*60,
                'task_type' => 1,
                'text' => "Перезвонить клиенту"
            ]
        ]);
    }

    /**
     * Валидация данных пришедших с формы
     *
     * @param $data
     * @return bool
     */
    private function validate($data)
    {
        $formValidator = new Form();
        return $formValidator->validate($data);
    }

}