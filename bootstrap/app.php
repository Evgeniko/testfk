<?php

/**
 * Файл предварительной конфигурации
 */

require __DIR__.'/../vendor/autoload.php';
$amo = require(__DIR__.'/../config/amo.php');

//Конфигурация приложения
$app = new \Slim\App([

   'settings' => [
       'displayErrorDetails' => true,
       'amo' => $amo,
       'cookie_path' => ROOT.'/storage/amo/cookie.txt',
       'admin_mail' => "testfabkliadm@gmail.com"
   ]

]);

$container = $app->getContainer();

//Определение контроллера в контейнер

$container['FormController'] = function ($container){
    return new \App\Controllers\FormController($container);
};



//Определение моделей в контейнер
$container['ContactModel'] = function ($container){
    return new \App\Models\Amo\Contact($container);
};

$container['AccountModel'] = function ($container){
    return new \App\Models\Amo\Account($container);
};


$container['LeadModel'] = function ($container){
    return new \App\Models\Amo\Lead($container);
};

$container['TaskModel'] = function ($container){
    return new \App\Models\Amo\Task($container);
};